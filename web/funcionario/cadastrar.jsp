<%-- 
    Document   : listar
    Created on : 24/04/2018, 11:50:57
    Author     : DualLayer
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="http://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
<!-- jQuery Toasty -->
<script src="../js/jquery.toast.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="../css/jquery.toast.min.css">

<div class="container-fluid bg-light p-0">
    <form id="formCadastro">
        <div class="card rounded-0">
            <div class="card-header">
                <h4>Cadastrar Funcionario</h4>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nome</span>
                                </div>
                                <input type="text" name="textNome" class="form-control" aria-label="Nome" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nascimento</span>
                                </div>
                                <input type="date" name="textNascimento" class="form-control" aria-label="Nascimento" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Email</span>
                                </div>
                                <input type="email" name="textEmail" class="form-control" aria-label="Email" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo do Logradouro</span>
                                </div>
                                <input type="text" name="textTipoLogradouro" class="form-control" aria-label="Tipo do Logradouro" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Logradouro</span>
                                </div>
                                <input type="text" name="textLogradouro" class="form-control" aria-label="Logradouro" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">N�mero</span>
                                </div>
                                <input type="text" name="textNumero" class="form-control" aria-label="N�mero" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">CEP</span>
                                </div>
                                <input type="text" name="textCEP" class="form-control" aria-label="CEP" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Bairro</span>
                                </div>
                                <input type="text" name="textBairro" class="form-control" aria-label="Bairro" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Cidade</span>
                                </div>
                                <input type="text" name="textCidade" class="form-control" aria-label="Cidade" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">UF</span>
                                </div>
                                <input type="text" name="textUF" class="form-control" aria-label="UF" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary col-sm-12">Cadastrar</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        var form = $('#formCadastro');
        form.submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: "../FrontController?action=GravarFuncionario",
                type: 'post',
                context: document.body,
                data: form.serialize(),
                success: function (data) {
                    if (data.substring(0, 4) === 'erro') {
                        var cabecalho = 'Erro!';
                        var tipo = 'error';
                        var mensagem = data.substring(4, 1000);
                        var time = 10000;

                    } else {
                        var cabecalho = 'Sucesso!';
                        var tipo = 'success';
                        var mensagem = data.substring(4, 1000);
                        var time = 3000;
                    }

                    $.toast({
                        text: mensagem, // Text that is to be shown in the toast
                        heading: cabecalho, // Optional heading to be shown on the toast
                        icon: tipo, // Type of toast icon
                        showHideTransition: 'fade', // fade, slide or plain
                        allowToastClose: true, // Boolean value true or false
                        hideAfter: time, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                        position: {left: 'auto', right: 30, top: 30, bottom: 'auto'}, // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                        textAlign: 'left', // Text alignment i.e. left, right or center
                        loader: true, // Whether to show loader or not. True by default
                        loaderBg: '#9EC600', // Background color of the toast loader
                        beforeShow: function () {}, // will be triggered before the toast is shown
                        afterShown: function () {}, // will be triggered after the toat has been shown
                        beforeHide: function () {}, // will be triggered before the toast gets hidden
                        afterHidden: function () {
                            location.reload();
                        }  // will be triggered after the toast has been hidden
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //what to do in error
                }
            });
        });
    });
</script>