<%-- 
    Document   : index
    Created on : 24/04/2018, 11:03:56
    Author     : Leoginski
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <title>Sistema de Folha de Pagamento</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="./img/favicon.ico">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    </head>

    <body>
        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>Folha de Pagamento</h3>
                    <strong>FP</strong>
                </div>

                <ul class="list-unstyled components">
                    <!--<li class="active">-->
                    <li>
                        <a href="index.jsp" aria-expanded="false">
                            <i class="fas fa-home"></i>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="#pageSubmenuFuncionario" data-toggle="collapse" aria-expanded="false">
                            <i class="fas fa-users"></i>
                            Funcionario
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenuFuncionario">
                            <li><a href="./FrontController?action=ListarFuncionario" target="frame">Listar</a></li>
                            <li><a href="./funcionario/cadastrar.jsp" target="frame">Cadastrar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenuContrato" data-toggle="collapse" aria-expanded="false">
                            <i class="fas fa-address-book"></i>
                            Contrato
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenuContrato">
                            <li><a href="./FrontController?action=ListarContrato" target="frame">Listar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenuDependente" data-toggle="collapse" aria-expanded="false">
                            <i class="fas fa-child"></i>
                            Dependente
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenuDependente">
                            <li><a href="./FrontController?action=ListarDependente" target="frame">Listar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenuSetor" data-toggle="collapse" aria-expanded="false">
                            <i class="fas fa-archive"></i>
                            Setor
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenuSetor">
                            <li><a href="./FrontController?action=ListarSetor" target="frame">Listar</a></li>
                            <li><a href="./FrontController?action=PreGravarSetor" target="frame">Cadastrar</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a target="__blank" href="https://github.com/Leoginski" class="download"><i class="fab fa-github"></i>Leoginski</a></li>
                    <li><a target="__blank" href="https://github.com/LucasLagrimante" class="download"><i class="fab fa-github"></i>LucasLagrimante</a></li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content" class="col-lg-12">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="fas fa-bars">   Menu</i>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><img src="./img/folha.png" width="50" height="50"></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="border-top border-bottom px-2">
                    <iframe id="iframe" name="frame" class="col-lg-12 p-0"></iframe>
                </div>
            </div>
        </div>

        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
        <!-- Latest compiled JavaScript -->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

        <!-- jQuery Toasty -->
        <script src="./js/jquery.toast.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="./css/jquery.toast.min.css">

        <link rel="stylesheet" href="./css/sidebar.css">

        <!-- Dual Layer -->
        <link rel="stylesheet" href="./css/style.css">
        <script src="./js/script.js" type="text/javascript"></script>
    </body>

</html>