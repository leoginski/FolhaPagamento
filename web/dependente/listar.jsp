<%-- 
    Document   : listar
    Created on : 24/04/2018, 11:50:57
    Author     : DualLayer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="http://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<!-- jQuery Toasty -->
<script src="./js/jquery.toast.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="./css/jquery.toast.min.css">

<div class="container-fluid bg-light p-0">

    <div class="card rounded-0">
        <div class="card-header">
            <h4>Dependentes</h4>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Nascimento</th>
                        <th scope="col">Funcionario</th>
                        <th scope="col">Tipo Dependente</th>
                        <th scope="col">Acao</th>
                    </tr>
                </thead>
                <tbody>

                    <c:forEach items="${dependentes}" var="dependente">
                        <tr>
                            <th scope="row">${dependente.id}</th>
                            <td>${dependente.nome}</td>
                            <td>${dependente.nascimento}</td>
                            <td>${dependente.dependido}</td>
                            <td>${dependente.tipoDependente}</td>
                            <td>
                                <a class="editar" href="./FrontController?action=GetDependente&id=${dependente.id}" target="frame"><i class="fas fa-edit"></i></a>
                                <a class="apagar" id="${dependente.id}" href="javascript:void(0);" target="frame"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('.apagar').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: "./FrontController?action=ExcluirDependente",
                type: "post",
                data: {
                    id: $(this).attr("id")
                },
                success: function (data) {
                    if (data.substring(0, 4) === 'erro') {
                        var cabecalho = 'Erro!';
                        var tipo = 'error';
                        var mensagem = data.substring(4, 1000);
                        var time = 10000;

                    } else {
                        var cabecalho = 'Sucesso!';
                        var tipo = 'success';
                        var mensagem = data.substring(4, 1000);
                        var time = 3000;
                    }

                    $.toast({
                        text: mensagem, // Text that is to be shown in the toast
                        heading: cabecalho, // Optional heading to be shown on the toast
                        icon: tipo, // Type of toast icon
                        showHideTransition: 'fade', // fade, slide or plain
                        allowToastClose: true, // Boolean value true or false
                        hideAfter: time, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                        stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                        position: {left: 'auto', right: 30, top: 30, bottom: 'auto'}, // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                        textAlign: 'left', // Text alignment i.e. left, right or center
                        loader: true, // Whether to show loader or not. True by default
                        loaderBg: '#9EC600', // Background color of the toast loader
                        beforeShow: function () {}, // will be triggered before the toast is shown
                        afterShown: function () {}, // will be triggered after the toat has been shown
                        beforeHide: function () {}, // will be triggered before the toast gets hidden
                        afterHidden: function () {
                            location.reload();
                        }  // will be triggered after the toast has been hidden
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //what to do in error
                }
            });
        });
    });
</script>