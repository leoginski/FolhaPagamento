/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Observable;
import java.util.Observer;
import state.FuncionarioEstadoDemitido;
import strategy.ContratoStrategy;

/**
 *
 * @author DualLayer
 */
public class Contrato extends ContratoStrategy implements Observer {

    private int id;
    private float salario;
    private Observable funcionario;

    public Contrato(int id, float salario, String contratado) {
        this.id = id;
        this.salario = salario;
        this.contratado = contratado;
    }

    public Contrato(float salario, String contratado) {
        this.salario = salario;
        this.contratado = contratado;
    }

    public Contrato() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    @Override
    public void update(Observable o, Object arg) {
        Funcionario funcionario = (Funcionario) o;
        if (funcionario.getEstado() instanceof FuncionarioEstadoDemitido) {
            this.setSalario(0);
        }
    }

    public Observable getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Observable funcionario) {
        this.funcionario = funcionario;
    }

}
