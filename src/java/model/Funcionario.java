/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import state.FuncionarioEstado;
import state.FuncionarioEstadoAfastado;
import state.FuncionarioEstadoAtivo;
import state.FuncionarioEstadoDemitido;

/**
 *
 * @author DualLayer
 */
public class Funcionario extends Observable {

    private int id;
    private String nome;
    private String email;
    private String nascimento;
    private int tipoLogradouro;
    private String logradouro;
    private int numero;
    private String CEP;
    private String bairro;
    private String cidade;
    private String UF;
    public FuncionarioEstado estado;
    public String memento;

    private String state;

    public Funcionario() {
    }

    public Funcionario(int id) {
        this.id = id;
    }

    public Funcionario(String nome, String email, String nascimento, int tipoLogradouro, String logradouro, int numero, String CEP, String bairro, String cidade, String UF) {
        this.nome = nome;
        this.email = email;
        this.nascimento = nascimento;
        this.tipoLogradouro = tipoLogradouro;
        this.logradouro = logradouro;
        this.numero = numero;
        this.CEP = CEP;
        this.bairro = bairro;
        this.cidade = cidade;
        this.UF = UF;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public int getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipo_logradouro(int tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public FuncionarioEstado getEstado() {
        return estado;
    }

    public void setEstado(FuncionarioEstado estado) {
        this.estado = estado;
        setChanged();
        notifyObservers();
    }

    public void setEstadoFromString(String estado) {
        if (estado.equals("Ativo")) {
            this.estado = new FuncionarioEstadoAtivo();
        } else if (estado.equals("Demitido")) {
            this.estado = new FuncionarioEstadoDemitido();
        } else if (estado.equals("Afastado")) {
            this.estado = new FuncionarioEstadoAfastado();
        }
    }

    public String getEstadoString() {
        return this.estado.getEstado();
    }

    public void setState(String string) {
        this.state = string;
    }

    public String getState() {
        return state;
    }

    public String getMemento() {
        return memento;
    }

    public String setLastMemento() {
        List<String> mementos = Arrays.asList(this.memento.split("\\;"));

        if (this.memento.length() > 0) {
            String newMementos = "";
            
            this.state = mementos.get(mementos.size() - 1);
            
            for (int i = 0; i < mementos.size() - 1; i++) {
                newMementos += mementos.get(i) + ";";
            }
            this.memento = newMementos;
            return "type=suce&response=Memento recuperado com sucesso. Novo estado: " + this.getState();
        }
        return "type=erro&response=Nao existem mementos para serem recuperados!";
    }

    public void setMemento(String memento) {
        this.memento += memento + ";";
    }

}
