/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import strategy.DependenteStrategy;

/**
 *
 * @author DualLayer
 */
public abstract class Dependente extends DependenteStrategy{

    protected int id;
    protected String nome;
    protected String nascimento;
    protected String tipo_dependente;
    protected int FK_funcionario;

    public Dependente() {
    }

    public String getDadosDependente() {
        return "Dependente: " + getNome() + " - Tipo Dependente: " + getTipoDependente();
    }

    public abstract String getTipoDependente();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public int getFK_funcionario() {
        return FK_funcionario;
    }

    public void setFK_funcionario(int FK_funcionario) {
        this.FK_funcionario = FK_funcionario;
    }

    public void setTipoDependente(String string) {
        this.tipo_dependente = string;
    }
}
