/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DualLayer
 */
public class DependenteFilho extends Dependente {

    public DependenteFilho() {
    }

    public DependenteFilho(String nome, String nascimento, int FK_funcionario) {
        this.nome = nome;
        this.nascimento = nascimento;
        this.FK_funcionario = FK_funcionario;
    }

    @Override
    public String getTipoDependente() {
        return "Filho ";
    }
}
