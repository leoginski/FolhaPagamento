/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Contrato;
import persistence.ContratoDAO;

/**
 *
 * @author DualLayer
 */
public class EditarContratoAction implements Action {

    public EditarContratoAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        int id = parseInt(request.getParameter("textId"));
        float salario = parseFloat(request.getParameter("textSalario"));

        if (request.getParameter("textId").equals("") || request.getParameter("textSalario").equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Contrato contrato = new Contrato();
            contrato.setId(id);
            contrato.setSalario(salario);
            try {
                ContratoDAO.getInstance().updateSalario(contrato);
                request.setAttribute("contratos", ContratoDAO.getInstance().getAll());
                RequestDispatcher view = request.getRequestDispatcher("./contrato/listar.jsp");
                view.forward(request, response);
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (ServletException ex) {
                Logger.getLogger(EditarContratoAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
