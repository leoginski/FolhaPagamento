/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Dependente;
import model.DependenteFilho;
import persistence.DependenteDAO;

/**
 *
 * @author DualLayer
 */
public class ExcluirDependenteAction implements Action {

    public ExcluirDependenteAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        
        try {
            DependenteDAO.getInstance().delete(parseInt(request.getParameter("id")));
            response.sendRedirect("return.jsp?type=suce&response=Excluido com sucesso!");
        } catch (ClassNotFoundException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        } catch (SQLException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }

}
