/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Funcionario;
import persistence.FuncionarioDAO;

/**
 *
 * @author DualLayer
 */
public class GravarFuncionarioAction implements Action {

    public GravarFuncionarioAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String nome = request.getParameter("textNome");
        String email = request.getParameter("textEmail");
        String nascimento = request.getParameter("textNascimento");
        int tipoLogradouro = parseInt(request.getParameter("textTipoLogradouro"));
        String logradouro = request.getParameter("textLogradouro");
        int numero = parseInt(request.getParameter("textNumero"));
        String cep = request.getParameter("textCEP");
        String bairro = request.getParameter("textBairro");
        String cidade = request.getParameter("textCidade");
        String uf = request.getParameter("textUF");

        if (nome.equals("") || email.equals("") || nascimento.equals("")
                || request.getParameter("textTipoLogradouro").equals("") || logradouro.equals("")
                || request.getParameter("textNumero").equals("") || cep.equals("") || bairro.equals("")
                || cidade.equals("") || uf.equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Funcionario funcionario = new Funcionario(nome, email, nascimento, tipoLogradouro, logradouro, numero, cep, bairro, cidade, uf);
            try {
                FuncionarioDAO.getInstance().save(funcionario, "Ativo");
                response.sendRedirect("return.jsp?type=suce&response=Gravado com sucesso!");
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            }
        }
    }

}
