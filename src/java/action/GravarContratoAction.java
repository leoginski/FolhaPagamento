/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Contrato;
import persistence.ContratoDAO;

/**
 *
 * @author DualLayer
 */
public class GravarContratoAction implements Action {

    public GravarContratoAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        float salario = parseFloat(request.getParameter("textSalario"));
        String FK_funcionario = request.getParameter("idFuncionario");

        if (request.getParameter("textSalario").equals("") || FK_funcionario.equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Contrato contrato = new Contrato(salario, FK_funcionario);
            try {
                ContratoDAO.getInstance().save(contrato);
                response.sendRedirect("return.jsp?type=suce&response=Gravado com sucesso!");
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            }
        }
    }

}
