/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Contrato;
import model.Funcionario;
import persistence.ContratoDAO;
import persistence.FuncionarioDAO;
import state.FuncionarioEstado;

/**
 *
 * @author DualLayer
 */
public class ChangeStateFuncionarioAction implements Action {

    public ChangeStateFuncionarioAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        int id = parseInt(request.getParameter("id"));
        String estado = request.getParameter("estado");
        String op = request.getParameter("op");
        String memento = request.getParameter("memento");

        Funcionario funcionario = new Funcionario(id);
        funcionario.memento = memento;
        funcionario.setEstadoFromString(estado);
        FuncionarioEstado estadoFuncionario = funcionario.getEstado();

        Contrato contrato = new Contrato();
        contrato.setContratado(request.getParameter("id"));
        contrato.setFuncionario(funcionario);

        String result = "";

        if (op.equals("ativar")) {
            result = estadoFuncionario.ativar(funcionario);
        } else if (op.equals("demitir")) {
            result = estadoFuncionario.demitir(funcionario);
        } else if (op.equals("afastar")) {
            result = estadoFuncionario.afastar(funcionario);
        }

        try {
            if (op.equals("updateMemento")) {
                result = funcionario.setLastMemento();
                FuncionarioDAO.getInstance().updateMemento(funcionario);
            } else {
                if (contrato.getSalario() == 0) {
                    ContratoDAO.getInstance().editar(contrato);
                }
                FuncionarioDAO.getInstance().updateEstado(funcionario);
            }

            response.sendRedirect("return.jsp?" + result);
        } catch (ClassNotFoundException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        } catch (SQLException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }

}
