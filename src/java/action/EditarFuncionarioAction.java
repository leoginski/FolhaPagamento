/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Funcionario;
import persistence.FuncionarioDAO;

/**
 *
 * @author DualLayer
 */
public class EditarFuncionarioAction implements Action {

    public EditarFuncionarioAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        int id = parseInt(request.getParameter("textId"));
        String nome = request.getParameter("textNome");
        String email = request.getParameter("textEmail");
        String nascimento = request.getParameter("textNascimento");
        int tipoLogradouro = parseInt(request.getParameter("textTipoLogradouro"));
        String logradouro = request.getParameter("textLogradouro");
        int numero = parseInt(request.getParameter("textNumero"));
        String cep = request.getParameter("textCEP");
        String bairro = request.getParameter("textBairro");
        String cidade = request.getParameter("textCidade");
        String uf = request.getParameter("textUF");

        if (request.getParameter("textId").equals("") || nome.equals("") || email.equals("") || nascimento.equals("")
                || request.getParameter("textTipoLogradouro").equals("") || logradouro.equals("")
                || request.getParameter("textNumero").equals("") || cep.equals("") || bairro.equals("")
                || cidade.equals("") || uf.equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Funcionario funcionario = new Funcionario(nome, email, nascimento, tipoLogradouro, logradouro, numero, cep, bairro, cidade, uf);
            funcionario.setId(id);
            try {
                FuncionarioDAO.getInstance().editar(funcionario);
                request.setAttribute("funcionarios", FuncionarioDAO.getInstance().getAll());
                RequestDispatcher view = request.getRequestDispatcher("./funcionario/listar.jsp");
                view.forward(request, response);
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (ServletException ex) {
                Logger.getLogger(EditarFuncionarioAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
