/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Dependente;
import model.DependenteFilho;
import persistence.DependenteDAO;

/**
 *
 * @author DualLayer
 */
public class EditarDependenteAction implements Action {

    public EditarDependenteAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        int id = parseInt(request.getParameter("textId"));
        String nome = request.getParameter("textNome");
        String nascimento = request.getParameter("textNascimento");
        String tipoDependente = request.getParameter("textTipoDependente");

        if (nome.equals("") || nascimento.equals("") || tipoDependente.equals("")
                || request.getParameter("textIdFuncionario").equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            try {
                Dependente dependente = new DependenteFilho();
                dependente.setId(id);
                dependente.setNome(nome);
                dependente.setNascimento(nascimento);
                DependenteDAO.getInstance().editar(dependente);
                request.setAttribute("dependentes", DependenteDAO.getInstance().getAll());
                RequestDispatcher view = request.getRequestDispatcher("./dependente/listar.jsp?");
                view.forward(request, response);
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (ServletException ex) {
                Logger.getLogger(EditarDependenteAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
