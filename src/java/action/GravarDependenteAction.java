/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Dependente;
import model.DependenteEsposa;
import model.DependenteFilho;
import persistence.DependenteDAO;

/**
 *
 * @author DualLayer
 */
public class GravarDependenteAction implements Action {
    
    public GravarDependenteAction() {
    }
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        String nome = request.getParameter("textNome");
        String nascimento = request.getParameter("textNascimento");
        String tipoDependente = request.getParameter("textTipoDependente");
        int idFuncionario = parseInt(request.getParameter("idFuncionario"));
        
        if (nome.equals("") || nascimento.equals("") || tipoDependente.equals("")
                || request.getParameter("idFuncionario").equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Dependente dependente = new DependenteFilho();
            
            if (tipoDependente.equals("Filho")) {
                dependente = new DependenteFilho(nome, nascimento, idFuncionario);
            } else {
                dependente = new DependenteEsposa(nome, nascimento, idFuncionario);
            }
            
            try {
                DependenteDAO.getInstance().save(dependente);
                response.sendRedirect("return.jsp?type=suce&response=Gravado com sucesso!");
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            }
        }
    }
    
}
