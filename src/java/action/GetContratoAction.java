/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Contrato;
import persistence.ContratoDAO;

/**
 *
 * @author DualLayer
 */
public class GetContratoAction implements Action {

    public GetContratoAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        try {
            request.setAttribute("contrato", ContratoDAO.getInstance().get(parseInt(request.getParameter("id"))));
            RequestDispatcher view = request.getRequestDispatcher("./contrato/editar.jsp");
            view.forward(request, response);
        } catch (ServletException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        } catch (ClassNotFoundException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }
}
