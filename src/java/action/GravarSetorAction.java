/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import chainofresponsability.Setor;
import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistence.SetorDAO;

/**
 *
 * @author DualLayer
 */
public class GravarSetorAction implements Action {

    public GravarSetorAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String nome = request.getParameter("textNome");
        int tipo_setor = parseInt(request.getParameter("textTipoSetor"));
        int id_funcionario = parseInt(request.getParameter("textIdFuncionario"));
        int id_setor = parseInt(request.getParameter("textIdSetor"));

        if (nome.equals("") || request.getParameter("textTipoSetor").equals("") || request.getParameter("textTipoSetor").equals("") || request.getParameter("textIdSetor").equals("")) {
            response.sendRedirect("return.jsp?type=erro&response=Algum campo esta vazio!!");
        } else {
            Setor setor = Setor.getObjectFromTipo(tipo_setor);
            setor.setFK_funcionario(id_funcionario);
            setor.setFK_setor(id_setor);
            setor.setNome(nome);
            try {
                SetorDAO.getInstance().save(setor);
                response.sendRedirect("return.jsp?type=suce&response=Gravado com sucesso!");
            } catch (ClassNotFoundException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            } catch (SQLException ex) {
                response.sendRedirect("return.jsp?type=erro&response=" + ex);
            }
        }
    }

}
