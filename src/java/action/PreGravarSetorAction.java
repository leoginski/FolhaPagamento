/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistence.SetorDAO;
import persistence.FuncionarioDAO;

/**
 *
 * @author DualLayer
 */
public class PreGravarSetorAction implements Action {

    public PreGravarSetorAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        try {
            request.setAttribute("setores", SetorDAO.getInstance().getAll());
            request.setAttribute("funcionarios", FuncionarioDAO.getInstance().getAll());
            RequestDispatcher view = request.getRequestDispatcher("./setor/cadastrar.jsp");
            view.forward(request, response);
        } catch (ServletException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PreGravarSetorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
