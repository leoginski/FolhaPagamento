/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import chainofresponsability.Relatorio;
import chainofresponsability.Setor;
import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistence.SetorDAO;

/**
 *
 * @author DualLayer
 */
public class ImprimirRelatorioAction implements Action {

    public ImprimirRelatorioAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        try {
            int tipoRelatorio = parseInt(request.getParameter("tipoRelatorio"));
            int idSetor = parseInt(request.getParameter("id"));
            
            Setor setor = SetorDAO.getInstance().get(idSetor);
            
            Relatorio relatorio = new Relatorio(tipoRelatorio);

            response.sendRedirect("return.jsp?" + setor.gerarRelatorio(relatorio));
        } catch (ClassNotFoundException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }
}
