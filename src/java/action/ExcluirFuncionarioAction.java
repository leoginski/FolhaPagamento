/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Funcionario;
import persistence.FuncionarioDAO;

/**
 *
 * @author DualLayer
 */
public class ExcluirFuncionarioAction implements Action {

    public ExcluirFuncionarioAction() {
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        Funcionario funcionario = new Funcionario(parseInt(request.getParameter("id")));
        try {
            FuncionarioDAO.getInstance().delete(funcionario);
            response.sendRedirect("return.jsp?type=suce&response=Excluido com sucesso!");
        } catch (ClassNotFoundException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        } catch (SQLException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }

}
