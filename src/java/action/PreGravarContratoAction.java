/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import controller.Action;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class PreGravarContratoAction implements Action {
    
    public PreGravarContratoAction() {
    }
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        
        int id = parseInt(request.getParameter("id"));
        String nome = request.getParameter("nome");
        
        Funcionario funcionario = new Funcionario();
        funcionario.setId(id);
        funcionario.setNome(nome);
        try {
            request.setAttribute("funcionario", funcionario);
            RequestDispatcher view = request.getRequestDispatcher("./contrato/cadastrar.jsp");
            view.forward(request, response);
        } catch (ServletException ex) {
            response.sendRedirect("return.jsp?type=erro&response=" + ex);
        }
    }
}
