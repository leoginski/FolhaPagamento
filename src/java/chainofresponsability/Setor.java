/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Funcionario;
import persistence.SetorDAO;

/**
 *
 * @author DualLayer
 */
public abstract class Setor {

    private int id;
    public String nome;
    private int tipoSetor;
    private String nomeTipoSetor;
    public String nomeSuperior;
    public String nomeResponsavel;
    private Setor setorSuperior;
    private int FK_funcionario;
    private int FK_setor;

    private Funcionario funcionarioResponsavel;
    protected ArrayList<TipoRelatorio> listaRelatorios = new ArrayList<TipoRelatorio>();

    public abstract String getDescricao();

    public String gerarRelatorio(Relatorio relatorio) throws SQLException, ClassNotFoundException {
        if (this.listaRelatorios.contains(relatorio.getTipoRelatorio())) {
            return "type=suce&response=" + relatorio.imprimeRelatorio(this);
        } else {
            if (!(this.getFK_setor() == 0)) {
                Setor superior = SetorDAO.getInstance().get(this.getFK_setor());
                return superior.gerarRelatorio(relatorio);
            } else {
                return "type=erro&response=O setor: " + this.nome + " nao possui responsavel ou superior para a impressao do " + relatorio.getTipoRelatorio().getDescricao() + "!";
            }
        }
    }

    public ArrayList getListaRelatorios() {
        return listaRelatorios;
    }

    public void setListaRelatorios(ArrayList listaRelatorios) {
        this.listaRelatorios = listaRelatorios;
    }

    public Funcionario getFuncionarioResponsavel() {
        return funcionarioResponsavel;
    }

    public void setFuncionarioResponsavel(Funcionario funcionarioResponsavel) {
        this.funcionarioResponsavel = funcionarioResponsavel;
    }

    public Setor getSetorSuperior() {
        return setorSuperior;
    }

    public void setSetorSuperior(Setor setorSuperior) {
        this.setorSuperior = setorSuperior;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipoSetor() {
        return tipoSetor;
    }

    public void setTipoSetor(int tipoSetor) {
        this.tipoSetor = tipoSetor;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeSuperior() {
        return nomeSuperior;
    }

    public void setNomeSuperior(String nomeSuperior) {
        this.nomeSuperior = nomeSuperior;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }

    public String getNomeTipoSetor() {
        return nomeTipoSetor;
    }

    public void setNomeTipoSetor(String nomeTipoSetor) {
        this.nomeTipoSetor = nomeTipoSetor;
    }

    public int getFK_funcionario() {
        return FK_funcionario;
    }

    public void setFK_funcionario(int FK_funcionario) {
        this.FK_funcionario = FK_funcionario;
    }

    public int getFK_setor() {
        return FK_setor;
    }

    public void setFK_setor(int FK_setor) {
        this.FK_setor = FK_setor;
    }

    public static Setor getObjectFromTipo(int tipo_setor) {
        switch (tipo_setor) {
            case 1:
                return new SetorProducao();
            case 2:
                return new SetorRecursosHumanos();
            case 3:
                return new SetorDiretoria();
        }
        return null;
    }

}
