/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

/**
 *
 * @author Leoginski
 */
public class RolRelatorios {

    private static RolRelatorios rolDocumentos = new RolRelatorios();
    private TipoRelatorioContabil tipoRelatorioContabil;
    private TipoRelatorioDesempenho tipoRelatorioDesempenho;
    private TipoRelatorioInvestimento tipoRelatorioInvestimento;

    public RolRelatorios() {
        tipoRelatorioContabil = new TipoRelatorioContabil();
        tipoRelatorioDesempenho = new TipoRelatorioDesempenho();
        tipoRelatorioInvestimento = new TipoRelatorioInvestimento();
    }

    public static RolRelatorios getInstance() {
        return rolDocumentos;
    }

    public static RolRelatorios getRolDocumentos() {
        return rolDocumentos;
    }

    public static void setRolDocumentos(RolRelatorios rolDocumentos) {
        RolRelatorios.rolDocumentos = rolDocumentos;
    }

    public TipoRelatorioContabil getTipoRelatorioContabil() {
        return tipoRelatorioContabil;
    }

    public void setTipoRelatorioContabil(TipoRelatorioContabil tipoRelatorioContabil) {
        this.tipoRelatorioContabil = tipoRelatorioContabil;
    }

    public TipoRelatorioDesempenho getTipoRelatorioDesempenho() {
        return tipoRelatorioDesempenho;
    }

    public void setTipoRelatorioDesempenho(TipoRelatorioDesempenho tipoRelatorioDesempenho) {
        this.tipoRelatorioDesempenho = tipoRelatorioDesempenho;
    }

    public TipoRelatorioInvestimento getTipoRelatorioInvestimento() {
        return tipoRelatorioInvestimento;
    }

    public void setTipoRelatorioInvestimento(TipoRelatorioInvestimento tipoRelatorioInvestimento) {
        this.tipoRelatorioInvestimento = tipoRelatorioInvestimento;
    }

}
