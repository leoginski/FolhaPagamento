/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class SetorRecursosHumanos extends Setor {

    public SetorRecursosHumanos(Funcionario responsavel, Setor setorSuperior) {
        this.setTipoSetor(2);
        this.setNome(this.getNome());
        this.setFuncionarioResponsavel(responsavel);
        this.setSetorSuperior(setorSuperior);
        this.setNomeTipoSetor("Recursos Humanos");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioContabil());
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioDesempenho());
    }

    SetorRecursosHumanos() {
        this.setTipoSetor(2);
        this.setNomeTipoSetor("Recursos Humanos");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioContabil());
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioDesempenho());
    }

    public String getDescricao() {
        return "Fonte do relatorio: Recursos Humanos - Responsavel: " + this.getFuncionarioResponsavel().getNome();
    }

}
