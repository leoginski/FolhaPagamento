/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class SetorDiretoria extends Setor {

    public SetorDiretoria(Funcionario responsavel, Setor setorSuperior) {
        this.setTipoSetor(3);
        this.setFuncionarioResponsavel(responsavel);
        this.setSetorSuperior(setorSuperior);
        this.setNomeTipoSetor("Diretoria");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioContabil());
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioInvestimento());
    }

    SetorDiretoria() {
        this.setTipoSetor(3);
        this.setNomeTipoSetor("Diretoria");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioContabil());
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioInvestimento());
    }

    @Override
    public String getDescricao() {
        return "Fonte do relatorio: Diretoria - Responsavel: " + this.getFuncionarioResponsavel().getNome();
    }
}
