/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

/**
 *
 * @author DualLayer
 */
public class Relatorio {

    private TipoRelatorio tipoRelatorio;

    public Relatorio(int tipoRelatorio) {
        TipoRelatorio tipoRelatorioObject = Relatorio.getObjectFromTipo(tipoRelatorio);
        this.setTipoRelatorio(tipoRelatorioObject);
    }

    public String imprimeRelatorio(Setor setor) {
        return "O " + tipoRelatorio.getDescricao() + " foi impresso pelo setor: " + setor.getNome() + " e assinado pelo responsavel: " + setor.getNomeResponsavel();
    }

    public Relatorio(TipoRelatorio tipoRelatorio) {
        this.tipoRelatorio = tipoRelatorio;
    }

    public TipoRelatorio getTipoRelatorio() {
        return tipoRelatorio;
    }

    public void setTipoRelatorio(TipoRelatorio tipoRelatorio) {
        this.tipoRelatorio = tipoRelatorio;
    }

    public static TipoRelatorio getObjectFromTipo(int tipoRelatorio) {
        switch (tipoRelatorio) {
            case 1:
                return RolRelatorios.getInstance().getTipoRelatorioDesempenho();
            case 2:
                return RolRelatorios.getInstance().getTipoRelatorioContabil();
            case 3:
                return RolRelatorios.getInstance().getTipoRelatorioInvestimento();
        }
        return null;
    }

}
