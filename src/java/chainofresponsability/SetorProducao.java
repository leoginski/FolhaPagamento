/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsability;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class SetorProducao extends Setor {

    public SetorProducao(Funcionario responsavel, Setor setorSuperior) {
        this.setTipoSetor(1);
        this.setFuncionarioResponsavel(responsavel);
        this.setSetorSuperior(setorSuperior);
        this.setNomeTipoSetor("Producao");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioDesempenho());
    }

    public SetorProducao() {
        this.setTipoSetor(1);
        this.setNomeTipoSetor("Producao");
        this.getListaRelatorios().add(RolRelatorios.getInstance().getTipoRelatorioDesempenho());
    }

    @Override
    public String getDescricao() {
        return "Fonte do relatorio: Producao - Responsavel: " + this.getFuncionarioResponsavel().getNome();
    }

}
