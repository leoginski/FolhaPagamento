/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public interface FuncionarioEstado {

    public String getEstado();

    public String afastar(Funcionario funcionario);

    public String demitir(Funcionario funcionario);

    public String ativar(Funcionario funcionario);
}
