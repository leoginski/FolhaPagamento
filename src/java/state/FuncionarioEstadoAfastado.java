/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class FuncionarioEstadoAfastado implements FuncionarioEstado {

    @Override
    public String getEstado() {
        return "Afastado";
    }

    @Override
    public String afastar(Funcionario funcionario) {
        return "type=erro&response=O funcionario ja esta afastado!";
    }

    @Override
    public String demitir(Funcionario funcionario) {
        funcionario.setEstado(new FuncionarioEstadoDemitido());
        funcionario.setMemento("Afastado");
        return "type=suce&response=O funcionario foi demitido com sucesso!";
    }

    @Override
    public String ativar(Funcionario funcionario) {
        funcionario.setEstado(new FuncionarioEstadoAtivo());
        funcionario.setMemento("Afastado");
        return "type=suce&response=O funcionario foi ativado com sucesso!";
    }

}
