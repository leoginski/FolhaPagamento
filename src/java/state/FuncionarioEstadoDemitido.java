/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class FuncionarioEstadoDemitido implements FuncionarioEstado {

    @Override
    public String getEstado() {
        return "Demitido";
    }

    @Override
    public String afastar(Funcionario funcionario) {
        return "type=erro&response=O funcionario nao pode ser afastado!";
    }

    @Override
    public String demitir(Funcionario funcionario) {
        return "type=erro&response=O funcionario ja esta demitido!";
    }

    @Override
    public String ativar(Funcionario funcionario) {
        funcionario.setEstado(new FuncionarioEstadoAtivo());
        funcionario.setMemento("Demitido");
        
        return "type=suce&response=O funcionario foi ativado com sucesso!";
    }
}
