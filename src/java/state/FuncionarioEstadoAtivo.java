/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class FuncionarioEstadoAtivo implements FuncionarioEstado {

    @Override
    public String getEstado() {
        return "Ativo";
    }

    @Override
    public String afastar(Funcionario funcionario) {
        funcionario.setEstado(new FuncionarioEstadoAfastado());
        funcionario.setMemento("Ativo");
        return "type=suce&response=O funcionario foi afastado com sucesso!";
    }

    @Override
    public String demitir(Funcionario funcionario) {
        funcionario.setEstado(new FuncionarioEstadoDemitido());
        funcionario.setMemento("Ativo");
        return "type=suce&response=O funcionario foi demitido com sucesso!!";
    }

    @Override
    public String ativar(Funcionario funcionario) {
        return "type=erro&response=O funcionario ja esta ativo!";
    }
}
