/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

/**
 *
 * @author DualLayer
 */
public abstract class DependenteStrategy {

    protected String dependido;

    public String getDependido() {
        return dependido;
    }

    public void setDependido(String dependido) {
        this.dependido = dependido;
    }
}
