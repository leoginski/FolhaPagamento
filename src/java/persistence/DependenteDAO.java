package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Dependente;
import model.DependenteEsposa;
import model.DependenteFilho;

/**
 *
 * @author DualLayer
 */
public class DependenteDAO {

    private static DependenteDAO instance = new DependenteDAO();

    public static DependenteDAO getInstance() {
        return instance;
    }

    private DependenteDAO() {
    }

    public void save(Dependente Dependente) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("INSERT INTO Dependente (nome, nascimento, tipo_dependente, FK_funcionario) "
                    + "VALUES ('"
                    + Dependente.getNome()
                    + "', '"
                    + Dependente.getNascimento()
                    + "', '"
                    + Dependente.getTipoDependente()
                    + "', '"
                    + Dependente.getFK_funcionario()
                    + "')");
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void delete(int id) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("DELETE FROM dependente WHERE id = " + id);
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public Dependente get(int id) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        Dependente dependenteResult = new DependenteFilho();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM dependente AS d INNER JOIN funcionario AS f ON d.FK_funcionario = f.id WHERE d.id = "
                    + id);
            while (rs.next()) {
                dependenteResult.setId(rs.getInt("d.id"));
                dependenteResult.setNome(rs.getString("d.nome"));
                dependenteResult.setNascimento(rs.getString("d.nascimento"));
                dependenteResult.setTipoDependente(rs.getString("d.tipo_dependente"));
                dependenteResult.setFK_funcionario(rs.getInt("d.FK_funcionario"));
                dependenteResult.setDependido(rs.getString("f.nome"));
            }

            return dependenteResult;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public List<Dependente> getAll() throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        List<Dependente> dependentesList = new ArrayList<Dependente>();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM dependente AS d INNER JOIN funcionario AS f ON d.FK_funcionario = f.id");

            while (rs.next()) {

                Dependente dependente = new DependenteFilho();

                if (rs.getString("d.tipo_dependente").equals("Esposa")) {
                    dependente = new DependenteEsposa();
                }

                dependente.setId(rs.getInt("d.id"));
                dependente.setNome(rs.getString("d.nome"));
                dependente.setNascimento(rs.getString("d.nascimento"));
                dependente.setTipoDependente(dependente.getTipoDependente());
                dependente.setFK_funcionario(rs.getInt("d.FK_funcionario"));
                dependente.setDependido(rs.getString("f.nome"));
                dependentesList.add(dependente);
            }

            return dependentesList;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void editar(Dependente dependente) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Dependente AS d"
                    + " SET d.nome = '"
                    + dependente.getNome()
                    + "', d.nascimento = '"
                    + dependente.getNascimento()
                    + "'"
                    + " WHERE d.id = "
                    + dependente.getId());

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void closeResourcer(Connection conn, Statement st) {
        try {
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }
}
