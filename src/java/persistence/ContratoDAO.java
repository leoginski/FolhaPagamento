package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Contrato;

/**
 *
 * @author DualLayer
 */
public class ContratoDAO {

    private static ContratoDAO instance = new ContratoDAO();

    public static ContratoDAO getInstance() {
        return instance;
    }

    private ContratoDAO() {
    }

    public void save(Contrato Contrato) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("INSERT INTO Contrato (salario, FK_funcionario) "
                    + "VALUES ('"
                    + Contrato.getSalario()
                    + "', '"
                    + Contrato.getContratado()
                    + "')");
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void delete(Contrato contrato) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("DELETE FROM contrato WHERE id = " + contrato.getId());
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public Contrato get(int id) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        Contrato contratoResult = new Contrato();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM contrato AS c INNER JOIN funcionario AS f ON f.id = c.FK_funcionario WHERE c.id = "
                    + id);
            while (rs.next()) {
                contratoResult.setId(rs.getInt("c.id"));
                contratoResult.setSalario(rs.getFloat("c.salario"));
                contratoResult.setContratado(rs.getString("f.nome"));
            }

            return contratoResult;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public List<Contrato> getAll() throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        List<Contrato> contratosList = new ArrayList<Contrato>();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM contrato AS c INNER JOIN funcionario AS f ON f.id = c.FK_funcionario");

            while (rs.next()) {
                Contrato contrato = new Contrato();
                contrato.setId(rs.getInt("c.id"));
                contrato.setSalario(rs.getFloat("c.salario"));
                contrato.setContratado(rs.getString("f.nome"));
                contratosList.add(contrato);
            }

            return contratosList;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void editar(Contrato Contrato) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Contrato AS c"
                    + " SET c.salario = '"
                    + Contrato.getSalario()
                    + "' WHERE c.FK_funcionario = "
                    + Contrato.getContratado());

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void updateSalario(Contrato Contrato) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Contrato AS c"
                    + " SET c.salario = '"
                    + Contrato.getSalario()
                    + "' WHERE c.id = "
                    + Contrato.getId());

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void closeResourcer(Connection conn, Statement st) {
        try {
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }
}
