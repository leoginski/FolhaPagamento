package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import chainofresponsability.Setor;
import chainofresponsability.SetorProducao;
import static java.lang.Integer.parseInt;

/**
 *
 * @author DualLayer
 */
public class SetorDAO {

    private static SetorDAO instance = new SetorDAO();

    public static SetorDAO getInstance() {
        return instance;
    }

    private SetorDAO() {
    }

    public void save(Setor Setor) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("INSERT INTO Setor (nome, tipo_setor, FK_funcionario, FK_setor) "
                    + "VALUES ('"
                    + Setor.getNome()
                    + "', '"
                    + Setor.getTipoSetor()
                    + "', '"
                    + Setor.getFK_funcionario()
                    + "', '"
                    + Setor.getFK_setor()
                    + "')");
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void delete(Setor setor) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("DELETE FROM setor WHERE id = " + setor.getId());
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public Setor get(int id) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            Setor setorResult = new SetorProducao();
            Setor superior;

            ResultSet rs = st.executeQuery("SELECT * FROM setor AS s INNER JOIN funcionario AS f ON f.id = s.FK_funcionario INNER JOIN setor AS superior ON s.id = superior.id WHERE s.id = "
                    + id);
            while (rs.next()) {
                setorResult = Setor.getObjectFromTipo(rs.getInt("s.tipo_setor"));
                if (rs.getString("s.FK_setor") != null) {            
                    setorResult.setNomeSuperior(rs.getString("superior.nome"));
                    setorResult.setFK_setor(parseInt(rs.getString("s.FK_setor")));
                } else {
                    setorResult.setNomeSuperior("Sem Superior");
                }
                setorResult.setId(rs.getInt("s.id"));
                setorResult.setNome(rs.getString("s.nome"));
                setorResult.setNomeResponsavel(rs.getString("f.nome"));
            }

            return setorResult;

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public List<Setor> getAll() throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        List<Setor> setorsList = new ArrayList<Setor>();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            Setor setor;
            Setor superior;

            ResultSet rs = st.executeQuery("SELECT * FROM setor AS s INNER JOIN funcionario AS f ON f.id = s.FK_funcionario LEFT JOIN setor AS superior ON superior.id = s.FK_setor");

            while (rs.next()) {
                setor = Setor.getObjectFromTipo(rs.getInt("s.tipo_setor"));
                if (rs.getString("s.FK_setor") != null) {
                    setor.setNomeSuperior(rs.getString("superior.nome"));
                } else {
                    setor.setNomeSuperior("Sem Superior");
                }

                setor.setId(rs.getInt("s.id"));
                setor.setNome(rs.getString("s.nome"));
                setor.setNomeResponsavel(rs.getString("f.nome"));
                setorsList.add(setor);
            }

            return setorsList;

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void editar(Setor Setor) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Setor AS s"
                    + " SET s.nome = '"
                    + Setor.getNome()
                    + "', SET s.FK_setor = '"
                    + Setor.getSetorSuperior().getId()
                    + "', s.FK_funcionario = '"
                    + Setor.getFuncionarioResponsavel().getId()
                    + "', s.tipo_setor = '"
                    + Setor.getTipoSetor()
                    + "' WHERE s.id = "
                    + Setor.getId());

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void closeResourcer(Connection conn, Statement st) {
        try {
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }
}
