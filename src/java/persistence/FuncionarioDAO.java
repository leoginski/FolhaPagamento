package persistence;

import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Funcionario;

/**
 *
 * @author DualLayer
 */
public class FuncionarioDAO {

    private static FuncionarioDAO instance = new FuncionarioDAO();

    public static FuncionarioDAO getInstance() {
        return instance;
    }

    private FuncionarioDAO() {
    }

    public void save(Funcionario funcionario, String estado) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("INSERT INTO funcionario (nome, nascimento, email,  tipo_logradouro, logradouro, numero, cep, bairro, cidade, uf, estado) "
                    + "VALUES ('"
                    + funcionario.getNome()
                    + "', '"
                    + funcionario.getNascimento()
                    + "', '"
                    + funcionario.getEmail()
                    + "', '"
                    + funcionario.getTipoLogradouro()
                    + "', '"
                    + funcionario.getLogradouro()
                    + "', '"
                    + funcionario.getNumero()
                    + "', '"
                    + funcionario.getCEP()
                    + "', '"
                    + funcionario.getBairro()
                    + "', '"
                    + funcionario.getCidade()
                    + "', '"
                    + funcionario.getUF()
                    + "', '"
                    + estado
                    + "')");
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void delete(Funcionario funcionario) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();
            st.execute("DELETE FROM funcionario WHERE id = " + funcionario.getId());
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public Funcionario get(Funcionario funcionario) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;
        Funcionario funcionarioResult = new Funcionario();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM funcionario AS f WHERE f.id = "
                    + funcionario.getId());
            while (rs.next()) {
                funcionarioResult.setId(rs.getInt("f.id"));
                funcionarioResult.setNome(rs.getString("f.nome"));
                funcionarioResult.setNascimento(rs.getString("f.nascimento"));
                funcionarioResult.setEmail(rs.getString("f.email"));
                funcionarioResult.setTipo_logradouro(parseInt(rs.getString("f.tipo_logradouro")));
                funcionarioResult.setLogradouro(rs.getString("f.logradouro"));
                funcionarioResult.setNumero(parseInt(rs.getString("f.numero")));
                funcionarioResult.setCEP(rs.getString("f.cep"));
                funcionarioResult.setCidade(rs.getString("f.cidade"));
                funcionarioResult.setBairro(rs.getString("f.bairro"));
                funcionarioResult.setUF(rs.getString("f.uf"));
                funcionario.setState(rs.getString("f.estado"));
                funcionario.memento = rs.getString("f.memento");
            }

            return funcionarioResult;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public List<Funcionario> getAll() throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        List<Funcionario> funcionariosList = new ArrayList<Funcionario>();

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM funcionario AS f");

            while (rs.next()) {
                Funcionario funcionario = new Funcionario();
                funcionario.setId(rs.getInt("f.id"));
                funcionario.setNome(rs.getString("f.nome"));
                funcionario.setNascimento(rs.getString("f.nascimento"));
                funcionario.setEmail(rs.getString("f.email"));
                funcionario.setState(rs.getString("f.estado"));
                funcionario.memento = rs.getString("f.memento");
                funcionariosList.add(funcionario);
            }

            return funcionariosList;
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void editar(Funcionario Funcionario) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Funcionario AS f"
                    + " SET nome = '"
                    + Funcionario.getNome()
                    + "', nascimento = '"
                    + Funcionario.getNascimento()
                    + "', email = '"
                    + Funcionario.getEmail()
                    + "', tipo_logradouro = '"
                    + Funcionario.getTipoLogradouro()
                    + "', logradouro = '"
                    + Funcionario.getLogradouro()
                    + "', numero = '"
                    + Funcionario.getNumero()
                    + "', cep = '"
                    + Funcionario.getCEP()
                    + "', bairro = '"
                    + Funcionario.getBairro()
                    + "', cidade = '"
                    + Funcionario.getCidade()
                    + "', uf = '"
                    + Funcionario.getUF()
                    + "'"
                    + " WHERE f.id = '"
                    + Funcionario.getId() + "'"
            );

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void updateMemento(Funcionario funcionario) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Funcionario AS f"
                    + " SET estado = '"
                    + funcionario.getState()
                    + "', memento = '"
                    + funcionario.getMemento()
                    + "'"
                    + " WHERE f.id = '"
                    + funcionario.getId() + "'"
            );

        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void updateEstado(Funcionario funcionario) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        Statement st = null;

        try {
            conn = DatabaseLocator.getInstance().getConnection();
            st = conn.createStatement();

            st.execute(
                    "UPDATE Funcionario AS f"
                    + " SET estado = '"
                    + funcionario.getEstado().getEstado()
                    + "', memento = '"
                    + funcionario.getMemento()
                    + "' WHERE f.id = '"
                    + funcionario.getId() + "'"
            );
        } catch (SQLException e) {
            throw e;
        } finally {
            closeResourcer(conn, st);
        }
    }

    public void closeResourcer(Connection conn, Statement st) {
        try {
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }
}
