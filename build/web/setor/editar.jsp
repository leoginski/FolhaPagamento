<%-- 
    Document   : editar
    Created on : 02/05/2018, 10:04:44
    Author     : Leoginski
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="http://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<!-- jQuery Toasty -->
<script src="./js/jquery.toast.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="./css/jquery.toast.min.css">

<div class="container-fluid bg-light p-0">
    <form action="./FrontController?action=EditarSetor" method="POST">
        <div class="card rounded-0">
            <div class="card-header">
                <h4>Editar Setor</h4>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nome</span>
                                </div>
                                <input type="text" name="textNome"  value="${setor.nome}" class="form-control" aria-label="Nome" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo Setor</span>
                                </div>
                                <input type="hidden" name="textId" value="${setor.id}" class="form-control" aria-label="IdSetor" aria-describedby="basic-addon1">
                                <input type="text" name="textTipoSetor" value="${setor.tipoSetor}" class="form-control" aria-label="Tipo Setor" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Funcionário Responsável</span>
                                </div>
                                <input type="text" name="textIdFuncionario" value="${setor.nomeResponsavel}" class="form-control" aria-label="Funcionario" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Setor Superior</span>
                                </div>
                                <input type="text" name="textIdSetor" value="${setor.nomeSuperior}" class="form-control" aria-label="Setor" aria-describedby="basic-addon1">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary col-sm-12">Salvar</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
