-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Abr-2018 a�s 15:08
-- Versa�o do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `folha`
--
CREATE DATABASE IF NOT EXISTS `folha` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `folha`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
CREATE TABLE IF NOT EXISTS `funcionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nascimento` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tipo_logradouro` int(11) NOT NULL,
  `logradouro` varchar(100) NOT NULL,
  `numero` int(11) NOT NULL,
  `cep` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(30) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `memento` varchar(1000),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `funcionario`
--

INSERT INTO `funcionario` (`nome`, `nascimento`, `email`, `tipo_logradouro`, `logradouro`, `numero`, `cep`, `bairro`, `cidade`, `uf`, `estado`) VALUES
('Lucas Lagrimante Martinho', '2018-05-04', 'lucaslagrimante@live.com', 1, 'Batista de Oliveira', 387, '36013-300', 'Centro', 'Juiz de Fora', 'MG', 'Ativo'),
('Leonardo Smoginski', '2018-05-04', 'leonardo.smoginski@gmail.com', 2, 'Hermano Teu', 45, '36013-300', 'Monte Castelo', 'Juiz de Fora', 'MG', 'Ativo');


-- --------------------------------------------------------

--
-- Estrutura da tabela `contrato`
--

DROP TABLE IF EXISTS `contrato`;
CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salario` float NOT NULL,
  `FK_funcionario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_funcionario_contrato` (`FK_funcionario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

--
-- Fazendo dump de dados para tabela `contrato`
--

INSERT INTO `contrato` (`salario`, `FK_funcionario`) VALUES
(15000.0, 1),
(15000.0, 2);
-- --------------------------------------------------------

--
-- Estrutura da tabela `dependente`
--

DROP TABLE IF EXISTS `dependente`;
CREATE TABLE IF NOT EXISTS `dependente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nascimento` varchar(100) NOT NULL,
  `tipo_dependente` varchar(100) NOT NULL,
  `FK_funcionario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_funcionario_dependente` (`FK_funcionario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

--
-- Fazendo dump de dados para tabela `dependente`
--

INSERT INTO `dependente` (`nome`, `nascimento`, `tipo_dependente`, `FK_funcionario`) VALUES
('Thais Mara�al', '2018-05-04', 'Esposa', 1),
('Barbara Heredia', '2018-05-04', 'Esposa', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor`
--

DROP TABLE IF EXISTS `setor`;
CREATE TABLE IF NOT EXISTS `setor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `tipo_setor` int(11) NOT NULL,
  `FK_funcionario` int(11) NOT NULL,
  `FK_setor` int(11),
  PRIMARY KEY (`id`),
  KEY `FK_funcionario_setor` (`FK_funcionario`),
  KEY `FK_setor_setor` (`FK_setor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

--
-- Fazendo dump de dados para tabela `setor`
--

INSERT INTO `setor` (`nome`, `tipo_setor`, `FK_funcionario`, `FK_setor`) VALUES
('Malharia', 1, 1, 2),
('Pagamento', 2, 2, 3),
('Chefia', 3, 1, null);
-- --------------------------------------------------------

--
-- Restria�a�es para tabelas `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `FK_funcionario_contrato` FOREIGN KEY (`FK_funcionario`) REFERENCES `funcionario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Restria�a�es para tabelas `dependente`
--
ALTER TABLE `dependente`
  ADD CONSTRAINT `FK_funcionario_dependente` FOREIGN KEY (`FK_funcionario`) REFERENCES `funcionario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Restria�a�es para tabelas `dependente_funcionario`
--
ALTER TABLE `setor`
  ADD CONSTRAINT `FK_funcionario_setor` FOREIGN KEY (`FK_funcionario`) REFERENCES `funcionario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Restria�a�es para tabelas `setor_setor`
--
ALTER TABLE `setor`
  ADD CONSTRAINT `FK_setor_setor` FOREIGN KEY (`FK_setor`) REFERENCES `setor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;